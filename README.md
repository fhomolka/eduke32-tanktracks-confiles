# Tank Tracks Con Files

## How to use

1. Extract all of your music into a separate folder - mine is named   `TankTracks`
2. Depending on your version, download the correct con file, and put it inside the `TankTracks` folder. If you need to replace any CON files, replace them
3. In the launcher select the `TankTracks` folder
4. Play!